var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index.ejs', { title: 'Express' });
});

router.post('/report', function(req, res, next) {
  res.render('report.ejs', { 
    idCard:req.body.idCard,
    prefixThai:req.body.prefixThai,
    nameThai:req.body.nameThai,
    lastnameThai:req.body.lastnameThai,
    nickname:req.body.nickname,
    birthDate:req.body.birthDate,
    province:req.body.province,
    district:req.body.district,
    district2:req.body.district2,
    address:req.body.address,
    road:req.body.road,
    postCode:req.body.postCode,
    tel:req.body.tel,
    tel2:req.body.tel2,
    tel3:req.body.tel3,
    email:req.body.email,
    prefixEng:req.body.prefixEng,
    nameEng:req.body.nameEng,
    lastnameEng:req.body.lastnameEng 
  });
});

module.exports = router;
